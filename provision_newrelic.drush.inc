<?php

/**
 * @file
 * Hookup with drush
 */

function provision_newrelic_provision_apache_vhost_config($data = null) {
  $config = new provisionConfig_drushrc_site(d()->name);
  drush_log(dt("add new relic variables to vhost"));
  return "<IfModule php5_module>
    php_value newrelic.appname \"". $config->uri .";Profile - ".$config->profile.";All Sites\"
    php_value newrelic.framework \"drupal\"
    php_value newrelic.framework.drupal.modules 1
  </IfModule>\n";
}

